const list = document.getElementById("list")
const commentContainer = document.getElementById("newComment")

let repId = [];

if(typeof localStorage.getItem('ireps') !== 'undefined' && localStorage.getItem('ireps') != null) {
  repId = JSON.parse("[" + localStorage.getItem("ireps") + "]")
}else {
  localStorage.setItem('ireps', [0])
  repId = repId = JSON.parse("[" + localStorage.getItem("ireps") + "]")
}

let drepId = [];

if(typeof localStorage.getItem('dreps') !== 'undefined' && localStorage.getItem('dreps') != null) {
  drepId = JSON.parse("[" + localStorage.getItem("dreps") + "]")
}else {
  localStorage.setItem('dreps', [0])
  drepId = drepId = JSON.parse("[" + localStorage.getItem("dreps") + "]")
}

window.addEventListener('DOMContentLoaded', () => {
    let requestURL = 'http://127.0.0.1:5500/data.json';
    let request = new XMLHttpRequest();
    request.open('GET', requestURL);
    request.responseType = 'json';
    request.send();
    request.onload = () => {
      if (typeof localStorage.getItem('state') !== 'undefined' && localStorage.getItem('state') != null) {
        console.log(localStorage.getItem('data'));
        let data = JSON.parse(localStorage.getItem('data'));
        document.getElementById("my-avatar").setAttribute("src", data.currentUser.image.png);
        
        listComments(data)
      } else {
        localStorage.setItem('state', "5")
        const data = request.response;
        document.getElementById("my-avatar").setAttribute("src", data.currentUser.image.png)
        listComments(data)
        localStorage.setItem('data', JSON.stringify(data))
      }
    }
    //rep control
    
})


function listComments(data) {
    list.innerHTML = '';
    const username = data.currentUser.username;
    for (let index = 0; index < data.comments.length; index++) {
        const comment = data.comments[index];
        let oldInner = list.innerHTML;
        oldInner = oldInner + `
        <li>
          <div>
            <span class="reputation-bar">
                <span class="increase" data-id="${comment.id}" onclick="increaseRep(${ comment.id })">
                <svg width="11" height="11" xmlns="http://www.w3.org/2000/svg"><path d="M6.33 10.896c.137 0 .255-.05.354-.149.1-.1.149-.217.149-.354V7.004h3.315c.136 0 .254-.05.354-.149.099-.1.148-.217.148-.354V5.272a.483.483 0 0 0-.148-.354.483.483 0 0 0-.354-.149H6.833V1.4a.483.483 0 0 0-.149-.354.483.483 0 0 0-.354-.149H4.915a.483.483 0 0 0-.354.149c-.1.1-.149.217-.149.354v3.37H1.08a.483.483 0 0 0-.354.15c-.1.099-.149.217-.149.353v1.23c0 .136.05.254.149.353.1.1.217.149.354.149h3.333v3.39c0 .136.05.254.15.353.098.1.216.149.353.149H6.33Z" fill="#C5C6EF"/></svg>
                </span>
                <span id="${"rep-" + comment.id}" class="rep">${ comment.score }</span>
                <span class="decrease" data-id="${comment.id}" onclick="decreaseRep(${ comment.id })">
                <svg width="11" height="3" xmlns="http://www.w3.org/2000/svg"><path d="M9.256 2.66c.204 0 .38-.056.53-.167.148-.11.222-.243.222-.396V.722c0-.152-.074-.284-.223-.395a.859.859 0 0 0-.53-.167H.76a.859.859 0 0 0-.53.167C.083.437.009.57.009.722v1.375c0 .153.074.285.223.396a.859.859 0 0 0 .53.167h8.495Z" fill="#C5C6EF"/></svg>
                </span>
            </span>
            <div class="comment">
                <span class="comment-header">
                <span class="user">
                    <span class="avatar">
            
                    <img src="${ comment.user.image.png }" alt="img">
                    </span>
                    <span class="username">
                    ${ comment.user.username }
                    </span>
                    `;
                    if(comment.user.username === username){
                        oldInner = oldInner + `
                        <span class="badge">you</span>
                        `
                    }
                    oldInner = oldInner + `
                    <span class="date">
                    ${ comment.createdAt }
                    </span>
                </span>
                `;
                if(comment.user.username === username){
                    oldInner = oldInner + `
                    <span data-id="${comment.id}" onclick="deleteComment(this)" class="delete">
                    <span>
                      Delete
                    </span>
                    <svg width="12" height="14" xmlns="http://www.w3.org/2000/svg"><path d="M1.167 12.448c0 .854.7 1.552 1.555 1.552h6.222c.856 0 1.556-.698 1.556-1.552V3.5H1.167v8.948Zm10.5-11.281H8.75L7.773 0h-3.88l-.976 1.167H0v1.166h11.667V1.167Z" fill="#ED6368"/></svg>
                  </span>
                  <span data-id="${comment.id}" onclick="initEdit(${comment.id})" class="edit">
                    <span>
                      Edit
                    </span>
                    <svg width="14" height="14" xmlns="http://www.w3.org/2000/svg"><path d="M13.479 2.872 11.08.474a1.75 1.75 0 0 0-2.327-.06L.879 8.287a1.75 1.75 0 0 0-.5 1.06l-.375 3.648a.875.875 0 0 0 .875.954h.078l3.65-.333c.399-.04.773-.216 1.058-.499l7.875-7.875a1.68 1.68 0 0 0-.061-2.371Zm-2.975 2.923L8.159 3.449 9.865 1.7l2.389 2.39-1.75 1.706Z" fill="#5357B6"/></svg>
                  </span>
                  <span id="${"comment-" + comment.id}" style="display: none">${JSON.stringify(comment)}</span>
                    `
                }else {
                    oldInner = oldInner + `
                    <span data-user="${comment.user.username}"  data-id="${comment.id}" onclick="replyInit(this)" class="reply">
                    <span>
                    Reply
                    </span>
                    <svg width="14" height="13" xmlns="http://www.w3.org/2000/svg"><path d="M.227 4.316 5.04.16a.657.657 0 0 1 1.085.497v2.189c4.392.05 7.875.93 7.875 5.093 0 1.68-1.082 3.344-2.279 4.214-.373.272-.905-.07-.767-.51 1.24-3.964-.588-5.017-4.829-5.078v2.404c0 .566-.664.86-1.085.496L.227 5.31a.657.657 0 0 1 0-.993Z" fill="#5357B6"/></svg>
                    </span>
                    `
                }
                oldInner = oldInner + `
                </span>
                <span class="comment-text">
                ${ comment.content }
                </span>
            </div>
          </div>
            `;
            if (typeof comment.replies != "undefined" && comment.replies != null && comment.replies.length != null && comment.replies.length > 0) {
                let arr = comment.replies;
                let oldReplies = `
                <ul>
                    <span class="vr"></span>
                `
                for (let index = 0; index < arr.length; index++) {
                    const replies = arr[index];
                    oldReplies = oldReplies + `
                    <li>
                    <span class="reputation-bar">
                      <span class="increase" data-id="${replies.id}" onclick="increaseRep(${ replies.id })">
                        <svg width="11" height="11" xmlns="http://www.w3.org/2000/svg"><path d="M6.33 10.896c.137 0 .255-.05.354-.149.1-.1.149-.217.149-.354V7.004h3.315c.136 0 .254-.05.354-.149.099-.1.148-.217.148-.354V5.272a.483.483 0 0 0-.148-.354.483.483 0 0 0-.354-.149H6.833V1.4a.483.483 0 0 0-.149-.354.483.483 0 0 0-.354-.149H4.915a.483.483 0 0 0-.354.149c-.1.1-.149.217-.149.354v3.37H1.08a.483.483 0 0 0-.354.15c-.1.099-.149.217-.149.353v1.23c0 .136.05.254.149.353.1.1.217.149.354.149h3.333v3.39c0 .136.05.254.15.353.098.1.216.149.353.149H6.33Z" fill="#C5C6EF"/></svg>
                      </span>
                      <span id="${"rep-" + replies.id}" class="rep">${ replies.score }</span>
                      <span class="decrease" data-id="${replies.id}" onclick="decreaseRep(${ replies.id })">
                        <svg width="11" height="3" xmlns="http://www.w3.org/2000/svg"><path d="M9.256 2.66c.204 0 .38-.056.53-.167.148-.11.222-.243.222-.396V.722c0-.152-.074-.284-.223-.395a.859.859 0 0 0-.53-.167H.76a.859.859 0 0 0-.53.167C.083.437.009.57.009.722v1.375c0 .153.074.285.223.396a.859.859 0 0 0 .53.167h8.495Z" fill="#C5C6EF"/></svg>
                      </span>
                    </span>
                    <div class="comment">
                      <span class="comment-header">
                        <span class="user">
                          <span data-type="reply-avatar" class="avatar">
                    
                            <img src="${ replies.user.image.png }" alt="img">
                          </span>
                          <span style="display: none"></span>
                          <span class="username">
                          ${ replies.user.username }
                          </span>
                          `;
                          if(replies.user.username === username) {
                              oldReplies = oldReplies + `
                              <span class="badge">you</span>
                              <span class="date">
                              ${ replies.createdAt }
                              </span>
                            </span>
                            <span data-id="${replies.id}" onclick="deleteComment(this)" class="delete">
                              <span>
                                Delete
                              </span>
                              <svg width="12" height="14" xmlns="http://www.w3.org/2000/svg"><path d="M1.167 12.448c0 .854.7 1.552 1.555 1.552h6.222c.856 0 1.556-.698 1.556-1.552V3.5H1.167v8.948Zm10.5-11.281H8.75L7.773 0h-3.88l-.976 1.167H0v1.166h11.667V1.167Z" fill="#ED6368"/></svg>
                            </span>
                            <span data-id="${replies.id}" class="edit" onclick="initEdit(${replies.id})">
                              <span>
                                Edit
                              </span>
                              <svg width="14" height="14" xmlns="http://www.w3.org/2000/svg"><path d="M13.479 2.872 11.08.474a1.75 1.75 0 0 0-2.327-.06L.879 8.287a1.75 1.75 0 0 0-.5 1.06l-.375 3.648a.875.875 0 0 0 .875.954h.078l3.65-.333c.399-.04.773-.216 1.058-.499l7.875-7.875a1.68 1.68 0 0 0-.061-2.371Zm-2.975 2.923L8.159 3.449 9.865 1.7l2.389 2.39-1.75 1.706Z" fill="#5357B6"/></svg>
                            </span>
                            <span id="${"comment-" + replies.id}" style="display: none">${JSON.stringify(replies)}</span>
                              `
                          } else {
                            oldReplies = oldReplies + `
                            <span class="date">
                            ${ replies.createdAt }
                            </span>
                            </span>
                            <span data-user="${replies.user.username}"  data-id="${comment.id}" onclick="replyInit(this)" class="reply">
                            <span>
                                Reply
                            </span>
                            <svg width="14" height="13" xmlns="http://www.w3.org/2000/svg"><path d="M.227 4.316 5.04.16a.657.657 0 0 1 1.085.497v2.189c4.392.05 7.875.93 7.875 5.093 0 1.68-1.082 3.344-2.279 4.214-.373.272-.905-.07-.767-.51 1.24-3.964-.588-5.017-4.829-5.078v2.404c0 .566-.664.86-1.085.496L.227 5.31a.657.657 0 0 1 0-.993Z" fill="#5357B6"/></svg>
                            </span>
                            `
                          }
                          oldReplies = oldReplies + `
                      </span>
                      <span class="comment-text">
                        <a href="#">${ "@" + replies.replyingTo }</a> ${ replies.content }
                      </span>
                    </div>
                  </li>
                    `
                }
                oldInner =  oldInner + oldReplies + `
                </ul>
                `
            }
        oldInner =  oldInner +`
        </li>
        `
        list.innerHTML = oldInner;
    }
    for (let index = 0; index < repId.length; index++) {
      let id = parseInt(repId[index])
      if(id != 0){
        updateHTML("rep-"+id, 1)
      }
    }
    for (let index = 0; index < drepId.length; index++) {
      let id = parseInt(drepId[index])
      if(id != 0){
        updateHTML("rep-"+id, 0)
      }
    }
}

console.log(repId);


function updateHTML(elmId, x) {
  var elem = document.getElementById(elmId);
  console.log(elem);
  if(typeof elem !== 'undefined' && elem !== null) {
    if(x===1){
      elem.previousElementSibling.innerHTML = `<svg width="11" height="11" xmlns="http://www.w3.org/2000/svg"><path d="M6.33 10.896c.137 0 .255-.05.354-.149.1-.1.149-.217.149-.354V7.004h3.315c.136 0 .254-.05.354-.149.099-.1.148-.217.148-.354V5.272a.483.483 0 0 0-.148-.354.483.483 0 0 0-.354-.149H6.833V1.4a.483.483 0 0 0-.149-.354.483.483 0 0 0-.354-.149H4.915a.483.483 0 0 0-.354.149c-.1.1-.149.217-.149.354v3.37H1.08a.483.483 0 0 0-.354.15c-.1.099-.149.217-.149.353v1.23c0 .136.05.254.149.353.1.1.217.149.354.149h3.333v3.39c0 .136.05.254.15.353.098.1.216.149.353.149H6.33Z" fill="#ed6468"/></svg>`
      elem.nextElementSibling.innerHTML = `<svg width="11" height="3" xmlns="http://www.w3.org/2000/svg"><path d="M9.256 2.66c.204 0 .38-.056.53-.167.148-.11.222-.243.222-.396V.722c0-.152-.074-.284-.223-.395a.859.859 0 0 0-.53-.167H.76a.859.859 0 0 0-.53.167C.083.437.009.57.009.722v1.375c0 .153.074.285.223.396a.859.859 0 0 0 .53.167h8.495Z" fill="#C5C6EF"/></svg>`

    }else {
      elem.previousElementSibling.innerHTML = `<svg width="11" height="11" xmlns="http://www.w3.org/2000/svg"><path d="M6.33 10.896c.137 0 .255-.05.354-.149.1-.1.149-.217.149-.354V7.004h3.315c.136 0 .254-.05.354-.149.099-.1.148-.217.148-.354V5.272a.483.483 0 0 0-.148-.354.483.483 0 0 0-.354-.149H6.833V1.4a.483.483 0 0 0-.149-.354.483.483 0 0 0-.354-.149H4.915a.483.483 0 0 0-.354.149c-.1.1-.149.217-.149.354v3.37H1.08a.483.483 0 0 0-.354.15c-.1.099-.149.217-.149.353v1.23c0 .136.05.254.149.353.1.1.217.149.354.149h3.333v3.39c0 .136.05.254.15.353.098.1.216.149.353.149H6.33Z" fill="#C5C6EF"/></svg>`
      elem.nextElementSibling.innerHTML = `<svg class="denemesc" width="11" height="3" xmlns="http://www.w3.org/2000/svg"><path d="M9.256 2.66c.204 0 .38-.056.53-.167.148-.11.222-.243.222-.396V.722c0-.152-.074-.284-.223-.395a.859.859 0 0 0-.53-.167H.76a.859.859 0 0 0-.53.167C.083.437.009.57.009.722v1.375c0 .153.074.285.223.396a.859.859 0 0 0 .53.167h8.495Z" fill="#ed6468"/></svg>`
    }
  }
}

function initEdit(id) {
  let currentData = document.getElementById("comment-"+id);
  currentData = JSON.parse(currentData.innerText);
  let btn = document.getElementById("btn");
  btn.setAttribute("data-id", id);
  btn.setAttribute("data-type", "edit");
  btn.innerText = "UPDATE"
  document.getElementById("myComment").value = currentData.content;
}

function replyInit(el){
  let replyingTo = el.attributes["data-user"].value;
  let id = el.attributes["data-id"].value;
  let btn = document.getElementById("btn");
  btn.setAttribute("data-id", id);
  btn.setAttribute("data-type", "reply");
  btn.innerText = "REPLY"
  commentContainer.innerHTML = `<a id="replying-tag" data-user="${replyingTo}" onclick="deleteReply()" href="#">${ "@" + replyingTo } <svg width="11" height="13" xmlns="http://www.w3.org/2000/svg"><path d="M1.167 12.448c0 .854.7 1.552 1.555 1.552h6.222c.856 0 1.556-.698 1.556-1.552V3.5H1.167v8.948Zm10.5-11.281H8.75L7.773 0h-3.88l-.976 1.167H0v1.166h11.667V1.167Z" fill="#fff"/></svg></a> <textarea id="myComment" placeholder="Add a comment..."></textarea>`
}

function deleteReply() {
  let btn = document.getElementById("btn");
  btn.setAttribute("data-id", "");
  btn.setAttribute("data-type", "add");
  btn.innerText = "SEND"
  commentContainer.innerHTML = `<textarea id="myComment" placeholder="Add a comment..."></textarea>`
}
function setComment(type){
  
  if(type.attributes["data-type"].value === "add"){
    addNewComment()
  }else if(type.attributes["data-type"].value === "edit"){
    editComment(document.getElementById("myComment").value, type.attributes["data-id"])
  }else if(type.attributes["data-type"].value === "reply"){
    let replyuser = document.getElementById("replying-tag")
    console.log("hmmm");
    replyComment(replyuser.attributes["data-user"].value, type.attributes["data-id"].value)
  } else {
    addNewComment();
  }
}

function addNewComment() {
  let comment = document.getElementById("myComment").value;
  let data = JSON.parse(localStorage.getItem('data'));
  var now = new Date().toLocaleTimeString();
  let dataId = parseInt(localStorage.getItem('state'))
  console.log("dataıd: " + dataId);
  data.comments.push({
    id : dataId,
    content : comment,
    createdAt: now,
    score: 0,
    user: {
      image : {
        png: data.currentUser.image.png,
        webp: data.currentUser.image.webp,
      },
      username: data.currentUser.username
    },
    replies: [],
  })
  dataId++;
  localStorage.setItem('state', dataId.toString())
  localStorage.setItem('data', JSON.stringify(data));
  listComments(data)
}

function deleteLocal(){
  localStorage.clear();
  location.reload()
}


function deleteComment(id) {
  id = parseInt(id.attributes["data-id"].value)
  let localData = JSON.parse(localStorage.getItem('data'));
  let dataId = parseInt(localStorage.getItem('state'))
  data = localData.comments;
  for (let index = 0; index < data.length; index++) {
    let comment = data[index];
    if (comment.id === id){
      data.splice(index, 1);
    }else if(typeof comment.replies != "undefined" && comment.replies != null && comment.replies.length != null && comment.replies.length > 0){
      let replies = comment.replies;
      for (let i = 0; i < replies.length; i++) {
        let rep = replies[i];
        if(rep.id === id){
          data[index].replies.splice(i, 1)
        }
      }
    }
  }
  localData.comments = data;
  localStorage.setItem("data", JSON.stringify(localData))
  listComments(localData);
  deleteReply()
}


function editComment(value, id){
  id = parseInt(id.value);
  let currentData = document.getElementById("comment-"+id);
  currentData = JSON.parse(currentData.innerText);
  let localData = JSON.parse(localStorage.getItem('data'));
  let dataId = parseInt(localStorage.getItem('state'))
  currentData.content = value;
  data = localData.comments;
  for (let index = 0; index < data.length; index++) {
    console.log("loop 1: " + index);
    let comment = data[index];
    if (comment.id === id){
      data.splice(index, 1, currentData);
    }else if(typeof comment.replies != "undefined" && comment.replies != null && comment.replies.length != null && comment.replies.length > 0){
      let replies = comment.replies;
      for (let i = 0; i < replies.length; i++) {
      console.log("loop 2: " + i);
        let rep = replies[i];
        if(rep.id === id){
          data[index].replies.splice(i, 1, currentData)
        }
      }
    }
  }
  localData.comments = data;
  localStorage.setItem("data", JSON.stringify(localData));
  listComments(localData);
  let btn = document.getElementById("btn");
  btn.setAttribute("data-type", "add");
  btn.setAttribute("data-id", "");
  btn.innerText = "SEND"

}

function replyComment(to, parentId){
  parentId = parseInt(parentId);
  let comment = document.getElementById("myComment").value;
  let data = JSON.parse(localStorage.getItem('data'));
  var now = new Date().toLocaleTimeString();
  let dataId = parseInt(localStorage.getItem('state'))

  for (let index = 0; index < data.comments.length; index++) {
    if(data.comments[index].id === parentId){
      
      data.comments[index].replies.push({
        id : dataId,
        content : comment,
        createdAt: now,
        score: 0,
        user: {
          image : {
            png: data.currentUser.image.png,
            webp: data.currentUser.image.webp,
          },
          username: data.currentUser.username
        },
        replyingTo: to,
      })
    }
  }
  dataId++;
  localStorage.setItem('state', dataId.toString())
  localStorage.setItem('data', JSON.stringify(data));
  listComments(data)
  deleteReply()
}


function increaseRep(id) {
  id = parseInt(id)
  let oldRep = parseInt(document.getElementById("rep-"+id).innerText);
  let control = 0;
  console.log(oldRep + " - oldrep");
  for (let index = 0; index < repId.length; index++) {
    if(repId[index] === id){
      console.log("başarısız zaten yaptın");
      control = 1;
    }
  }
  if(control === 0){
    console.log("rep + başarılı");
    oldRep++;
    console.log(oldRep + " başarılı oldrep");
    document.getElementById("rep-"+id).innerText = oldRep;
    console.log(document.getElementById("rep-"+id).innerText);
    for (let index = 0; index < drepId.length; index++) {
      if(drepId[index] === id) {
        drepId.splice(index, 1);
      }else if(index === drepId.length - 1){
        repId.push(id);
      }
    }
    let data = JSON.parse(localStorage.getItem('data'));
    for (let index = 0; index < data.comments.length; index++) {
      if(data.comments[index].id === id){
        data.comments[index].score++
      }else if(typeof data.comments[index].replies != "undefined" && data.comments[index].replies != null && data.comments[index].replies.length != null && data.comments[index].replies.length > 0){
        for (let i = 0; i < data.comments[index].replies.length; i++) {
          let rep = data.comments[index].replies[i];
          
          if(rep.id === id){
            data.comments[index].replies[i].score++
            console.log("score + :" + data.comments[index].replies[i].score);
          }
        }
      }
    }
    
  localStorage.setItem('data', JSON.stringify(data));
  listComments(data)
  localStorage.setItem("ireps", repId)
  localStorage.setItem("dreps", drepId)
  }
}


function decreaseRep(id) {
  id = parseInt(id)
  let oldRep = parseInt(document.getElementById("rep-"+id).innerText);
  let control = 0;
  for (let index = 0; index < drepId.length; index++) {
    if(drepId[index] === id){
      console.log("başarısız zaten azaltma yaptın");
      control = 1;
    }
  }
  if(control === 0){
    console.log("rep - başarılı");
    oldRep--;
    document.getElementById("rep-"+id).innerText = oldRep;
    for (let index = 0; index < repId.length; index++) {
      if(repId[index] === id) {
        repId.splice(index, 1);
      }else if(index === repId.length - 1){
        drepId.push(id);
        document.getElementById("rep-"+id).nextElementSibling.innerHTML = `<svg class="denemesc" width="11" height="3" xmlns="http://www.w3.org/2000/svg"><path d="M9.256 2.66c.204 0 .38-.056.53-.167.148-.11.222-.243.222-.396V.722c0-.152-.074-.284-.223-.395a.859.859 0 0 0-.53-.167H.76a.859.859 0 0 0-.53.167C.083.437.009.57.009.722v1.375c0 .153.074.285.223.396a.859.859 0 0 0 .53.167h8.495Z" fill="#ed6468"/></svg>`
        console.log(document.getElementById("rep-"+id).nextElementSibling.firstElementChild.style.fill);
      }
    }
    let data = JSON.parse(localStorage.getItem('data'));
    for (let index = 0; index < data.comments.length; index++) {
      if(data.comments[index].id === id){
        data.comments[index].score--
      }else if(typeof data.comments[index].replies != "undefined" && data.comments[index].replies != null && data.comments[index].replies.length != null && data.comments[index].replies.length > 0){
        for (let i = 0; i < data.comments[index].replies.length; i++) {
          let rep = data.comments[index].replies[i];
          if(rep.id === id){
            data.comments[index].replies[i].score--
            console.log("score - :" + data.comments[index].replies[i].score);
          }
        }
      }
    }
  localStorage.setItem('data', JSON.stringify(data));
  listComments(data)

  localStorage.setItem("ireps", repId)
  localStorage.setItem("dreps", drepId)
  }
}





// for (let index = 0; index < repId.length; index++) {
//   let id = parseInt(repId[index])
//   if(id != 0){
//     updateHTML("rep-"+id)
//     document.getElementById("rep-"+id).nextElementSibling.innerHTML = `<svg width="11" height="11" xmlns="http://www.w3.org/2000/svg"><path d="M6.33 10.896c.137 0 .255-.05.354-.149.1-.1.149-.217.149-.354V7.004h3.315c.136 0 .254-.05.354-.149.099-.1.148-.217.148-.354V5.272a.483.483 0 0 0-.148-.354.483.483 0 0 0-.354-.149H6.833V1.4a.483.483 0 0 0-.149-.354.483.483 0 0 0-.354-.149H4.915a.483.483 0 0 0-.354.149c-.1.1-.149.217-.149.354v3.37H1.08a.483.483 0 0 0-.354.15c-.1.099-.149.217-.149.353v1.23c0 .136.05.254.149.353.1.1.217.149.354.149h3.333v3.39c0 .136.05.254.15.353.098.1.216.149.353.149H6.33Z" fill="#ed6468"/></svg>`
//   }
// }
// for (let index = 0; index < drepId.length; index++) {
//   let id = parseInt(drepId[index])
//   if(id != 0){
//     console.log(id);
//     document.getElementById("rep-"+id).nextElementSibling.innerHTML = `<svg class="denemesc" width="11" height="3" xmlns="http://www.w3.org/2000/svg"><path d="M9.256 2.66c.204 0 .38-.056.53-.167.148-.11.222-.243.222-.396V.722c0-.152-.074-.284-.223-.395a.859.859 0 0 0-.53-.167H.76a.859.859 0 0 0-.53.167C.083.437.009.57.009.722v1.375c0 .153.074.285.223.396a.859.859 0 0 0 .53.167h8.495Z" fill="#ed6468"/></svg>`
//   }
// }

